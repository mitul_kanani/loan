<style>
    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 20px;
        display: table;
    }

    .text-left {
        text-align: left;
    }

    .text-right {
        text-align: right;
    }

    .text-center {
        text-align: center;
    }

    .text-justify {
        text-align: justify;
    }

    .pull-right {
        float: right !important;
    }

    span {
        font-weight: bold;
    }

    table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 20px;
    }
</style>


<div>
    <table>
        <tr style="margin-top: 15px;">
            <th colspan="6">
                <h2 style="margin-bottom: 0px;">
                    {{\App\Models\Setting::where('setting_key','company_name')->first()->setting_value}}
                </h2>
            </th>
        </tr>
        <tr>
            <td colspan="6">
                {{\App\Models\Setting::where('setting_key','company_address')->first()->setting_value}}
            </td>
        </tr>
        <tr>
            <td colspan="6">
                Tel - {{\App\Models\Setting::where('setting_key','company_phone')->first()->setting_value}}
            </td>
        </tr>
        <tr style="margin-top: 15px;">
            <td colspan="4"></td>
            <td colspan="2"><h3 style="margin-bottom: 10px;">Reciept</h3></td>
        </tr>
        <tr><td colspan="6"><hr/></td></tr>
        <tr>
            <td colspan="2"><b>File No :</b> {{$loan_transaction->loan->loan_number}}</td>
            <td colspan="2">
                <b>Reciept No :</b>
                @if(!empty($loan_transaction->receipt))
                    {{$loan_transaction->receipt}}
                @endif
            </td>
            <td colspan="2">
                <b>Date :</b>
                {{$loan_transaction->date}}
            </td>
        </tr>
        <tr style="margin-top: 10px;margin-bottom: 10px;">
            <td colspan="3"><br/><b>Recieve with Thanks From Mr/Mrs.</b></td>
            <td colspan="3"><br/>{{$loan_transaction->borrower->first_name}} {{$loan_transaction->borrower->last_name}}</td>
        </tr>
        <tr style="margin-top: 10px;margin-bottom: 10px;">
            <td colspan="2"><br/><b>The Sum of rupees</b></td>
            <td colspan="4"><br/>
                @if($loan_transaction->credit>$loan_transaction->debit)
                    {{\App\Helpers\GeneralHelper::get_words_from_number($loan_transaction->credit,2)}}
                @else
                    {{\App\Helpers\GeneralHelper::get_words_from_number($loan_transaction->debit,2)}}
                @endif
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br/><br/><br/><br/><br/>
                <hr />Cashier
            </td>
            <td></td>
            <td colspan="3">
                <br/>
                <table class="nested_table"  style="margin-left:15px;">
                    @php
                        $charges = 0;
                        foreach($custom_fields as $key) {
                            if(!empty($key->custom_field)) {
                                $charges = $charges + $key->name;
                            }
                        }

                        $interest = $penalities = 0;
                        if(!empty($schedules)) {
                            $interest = $schedules->interest;
                            $penalities = $schedules->penalty + $schedules->penalty_2;
                        }

                        $total_amount = 0;
                        if($loan_transaction->credit>$loan_transaction->debit) {
                            $total_amount = $loan_transaction->credit;
                        } else {
                            $total_amount = $loan_transaction->debit;
                        }

                        $installment = $total_amount - $charges - $interest - $penalities;

                    @endphp
                    <tr>
                        <td>Installment</td>
                        <td>{{ number_format($installment,2) }}</td>
                    </tr>
                    <tr>
                        <td>Interest</td>
                        <td>{{ number_format($interest,2) }}</td>
                    </tr>
                    <tr>
                        <td>Delay Charges</td>
                        <td>{{ number_format($penalities,2) }}</td>
                    </tr>
                    @foreach($custom_fields as $key)
                        @if(!empty($key->custom_field))
                            <tr>
                                <td>{{$key->custom_field->name}}</td>
                                <td>
                                    @if($key->name)
                                        {{$key->name}}
                                    @else
                                        0.00
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    <tr>
                        <td><b>Total</b></td>
                        <td><b>
                            @if($loan_transaction->credit>$loan_transaction->debit)
                                {{number_format($loan_transaction->credit,2)}}
                            @else
                                {{number_format($loan_transaction->debit,2)}}
                            @endif
                        </b></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

