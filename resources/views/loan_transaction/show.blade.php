@extends('layouts.master')
@section('title'){{trans_choice('general.loan',1)}} {{trans_choice('general.transaction',1)}}
@endsection
@section('content')
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">{{trans_choice('general.loan',1)}} {{trans_choice('general.transaction',1)}}</h6>
            <div class="heading-elements"></div>
        </div>
        <div class="panel-body" style="padding-top:0px;">
            <div class="row">
                <div class="col-md-12">
                    <h2 style="margin-bottom: 0px;">{{\App\Models\Setting::where('setting_key','company_name')->first()->setting_value}}</h2>
                    <p style="margin-bottom: 0px;">{{\App\Models\Setting::where('setting_key','company_address')->first()->setting_value}}</p>
                    <p style="margin-bottom: 0px;">Tel - {{\App\Models\Setting::where('setting_key','company_phone')->first()->setting_value}}</p>
                </div>
            </div>
            <div class="row">    
                <div class="col-md-offset-8">
                    <h3 style="margin-top: 0px;margin-bottom: 0px;margin-left: 20px;">Reciept</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <hr style="margin-top: 10px" />
                    <div class="col-md-4">
                        <p><b>File No :</b> {{$loan_transaction->loan->loan_number}}</p>
                    </div>
                    <div class="col-md-4">
                        <p style="margin-left:20px;"><b>Reciept No :</b>
                        @if(!empty($loan_transaction->receipt))
                            {{$loan_transaction->receipt}}
                        @endif
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p><b>Date :</b> {{$loan_transaction->date}}</p>
                    </div>  
                </div>
                <div class="clearfix"></div>
                <br/>
                <div class="col-md-12">
                    <div class="col-md-3">
                        <p><b>Recieve with Thanks From Mr/Mrs.</b></p>
                    </div>
                    <div class="col-md-9">
                        <p>{{$loan_transaction->borrower->first_name}} {{$loan_transaction->borrower->last_name}}</p>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br/>
                @if($loan_transaction->transaction_type=='repayment')
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <p><b>The Sum of rupees</b></p>
                            <div>
                                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                                <hr/>Cashier
                            </div>
                        </div>
                        <div class="col-md-4">
                            <p>@if($loan_transaction->credit>$loan_transaction->debit)
                                {{\App\Helpers\GeneralHelper::get_words_from_number($loan_transaction->credit,2)}}
                            @else
                                {{\App\Helpers\GeneralHelper::get_words_from_number($loan_transaction->debit,2)}}
                            @endif</p>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-striped table-hover">
                                @php
                                    $charges = 0;
                                    foreach($custom_fields as $key) {
                                        if(!empty($key->custom_field)) {
                                            $charges = $charges + $key->name;
                                        }
                                    }

                                    $interest = $penalities = 0;
                                    if(!empty($schedules)) {
                                        $interest = $schedules->interest;
                                        $penalities = $schedules->penalty + $schedules->penalty_2;
                                    }

                                    $total_amount = 0;
                                    if($loan_transaction->credit>$loan_transaction->debit) {
                                        $total_amount = $loan_transaction->credit;
                                    } else {
                                        $total_amount = $loan_transaction->debit;
                                    }

                                    $installment = $total_amount - $charges - $interest - $penalities;

                                @endphp
                                <tr>
                                    <td>Installment</td>
                                    <td>{{ number_format($installment,2) }}</td>
                                </tr>
                                <tr>
                                    <td>Interest</td>
                                    <td>{{ number_format($interest,2) }}</td>
                                </tr>
                                <tr>
                                    <td>Delay Charges</td>
                                    <td>{{ number_format($penalities,2) }}</td>
                                </tr>
                                @foreach($custom_fields as $key)
                                    @if(!empty($key->custom_field))
                                        <tr>
                                            <td>{{$key->custom_field->name}}</td>
                                            <td>
                                                @if($key->name)
                                                    {{$key->name}}
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                <tr>
                                    <td>Total</td>
                                    <td>
                                        @if($loan_transaction->credit>$loan_transaction->debit)
                                            {{number_format($loan_transaction->credit,2)}}
                                        @else
                                            {{number_format($loan_transaction->debit,2)}}
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                @else
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <p><b>Amount</b></p>
                        </div>
                        <div class="col-md-9">
                            @if($loan_transaction->credit>$loan_transaction->debit)
                                {{number_format($loan_transaction->credit,2)}}
                            @else
                                {{number_format($loan_transaction->debit,2)}}
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br/>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <p><b>Amount in words</b></p>
                        </div>
                        <div class="col-md-9">
                            @if($loan_transaction->credit>$loan_transaction->debit)
                                {{\App\Helpers\GeneralHelper::get_words_from_number($loan_transaction->credit,2)}}
                            @else
                                {{\App\Helpers\GeneralHelper::get_words_from_number($loan_transaction->debit,2)}}
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <br/><br/><br/><br/>
                            <hr/>Cashier
                        </div>
                        <div class="col-md-10">
                        </div>
                    </div>    
                @endif
            </div>
            
        </div>
        <!-- /.panel-body -->
        <div class="panel-footer">
            <div class="heading-elements">
                <a href="{{ url()->previous() }}"  class="btn btn-primary pull-right">{{trans_choice('general.back',1)}}</a>
            </div>
        </div>
    </div>
    <!-- /.box -->
@endsection

