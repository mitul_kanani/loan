@extends('layouts.master')
@section('title')
    {{ trans_choice('general.brance_limit',1) }}
@endsection
@section('content')
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">{{ trans_choice('general.brance_limit',1) }}</h6>

            <div class="heading-elements">

            </div>
        </div>
        {!! Form::open(array('url' => 'user/limit_update','class'=>'form-horizontal',"enctype" => "multipart/form-data")) !!}
        <div class="panel-body">
            <div class="form-group">
                {!! Form::label('brance_limit',trans_choice('general.brance_limit',1),array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-6">
                   <input type="number" name="brance_limit" value="{{$limit}}" class="form-control">
                </div>
            </div>
        </div>
        <!-- /.panel-body -->
        <div class="panel-footer">
            <div class="heading-elements">
                <button type="submit" class="btn btn-primary pull-right">{{ trans_choice('general.save',1) }}</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection

