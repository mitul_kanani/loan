@extends('layouts.master')
@section('title')
    {{ trans_choice('general.login_footer',1) }}
@endsection
@section('content')
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">{{ trans_choice('general.login_footer',1) }}</h6>

            <div class="heading-elements">

            </div>
        </div>
        {!! Form::open(array('url' => 'user/update_footer_status','class'=>'form-horizontal',"enctype" => "multipart/form-data")) !!}
        <div class="panel-body">
             <div class="col-md-8">
                @if(Sentinel::getUser()->status==1)
                    <div class="form-group">
                        {!! Form::label('login_footer',trans('general.login_footer'),array('class'=>'col-sm-2 control-label')) !!}
                        <div class="col-sm-10">
                            {!! Form::select('login_footer',array('enable'=>trans('general.enable'),'disable'=>trans('general.disable')),\App\Models\Setting::where('setting_key','login_footer')->first()->setting_value,array('class'=>'form-control','required'=>'required')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('footer_information',trans('general.footer_information'),array('class'=>'col-sm-2 control-label')) !!}
                        <div class="col-sm-10">
                            {!! Form::text('footer_information',\App\Models\Setting::where('setting_key','footer_information')->first()->setting_value,array('class'=>'form-control','required'=>'required')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('system_version',trans('general.system_version'),array('class'=>'col-sm-2 control-label')) !!}
                        <div class="col-sm-10">
                            {!! Form::text('system_version',\App\Models\Setting::where('setting_key','system_version')->first()->setting_value,array('class'=>'form-control','required'=>'required')) !!}
                        </div>
                    </div>
                        
               @endif
            </div>
        </div>
        <!-- /.panel-body -->
        <div class="panel-footer">
            <div class="heading-elements">
                <button type="submit" class="btn btn-primary pull-right">{{ trans_choice('general.save',1) }}</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection





























