@extends('layouts.master')
@section('title')
    {{ trans_choice('general.edit',1) }} {{ trans_choice('general.user',1) }}
@endsection
@section('content')
    <div class="box box-default">
        <div class="panel-heading">
            <h6 class="panel-title"> {{ trans_choice('general.edit',1) }} {{ trans_choice('general.user',1) }}</h6>

            <div class="heading-elements">

            </div>
        </div>
        {!! Form::open(array('url' => 'user/profile','class'=>'form-horizontal form-bordered form-label-stripped',"enctype" => "multipart/form-data")) !!}

        <div class="panel-body">
            <div class="form-group">
                {!!  Form::label(trans('general.first_name'),null,array('class'=>'control-label')) !!}

                {!! Form::text('first_name',$user->first_name,array('class'=>'form-control','required'=>'required')) !!}
            </div>
            <div class="form-group">
                {!!  Form::label(trans('general.last_name'),null,array('class'=>'control-label')) !!}
                {!! Form::text('last_name',$user->last_name,array('class'=>'form-control','required'=>'required')) !!}
            </div>
            <div class="form-group">
                {!!  Form::label(trans('general.gender'),null,array('class'=>' control-label')) !!}
                {!! Form::select('gender', array('Male' =>trans('general.male'), 'Female' => trans('general.female')),$user->gender,array('class'=>'form-control')) !!}
            </div>
            <div class="form-group">
                {!!  Form::label(trans('image'),null,array('class'=>' control-label')) !!}
                @php
                if(isset($user->image))
                    $pic=asset('/assets/images'.'/'.$user->image);
                else{
                    $pic="";
                }
                @endphp
                <div>
                    <img style="width: 150px;height: 200px" id="preview"  src="{{$pic}}" alt="User Image">
                </div>
                <div style="text-align: left;margin-top: 5px">
                    <input class="btn btn-primary" id="usr_img"  onchange="readURL(this);" accept="image/*" name="user_image" type="file" >
                </div>

            </div>
            <div class="form-group">
                {!!  Form::label(trans('general.phone'),null,array('class'=>'control-label')) !!}
                {!! Form::text('phone',$user->phone,array('class'=>'form-control')) !!}
            </div>
            <div class="form-group ">
                {!!  Form::label(trans_choice('general.email',1),null,array('class'=>'control-label')) !!}
                {!! Form::email('email',$user->email,array('class'=>'form-control','required'=>'required')) !!}
            </div>
            <div class="form-group">
                {!!  Form::label(trans('general.password'),null,array('class'=>'control-label')) !!}
                {!! Form::password('password',array('class'=>'form-control')) !!}
            </div>
            <div class="form-group">
                {!!  Form::label(trans('general.repeat_password'),null,array('class'=>'control-label')) !!}
                {!! Form::password('rpassword',array('class'=>'form-control')) !!}
            </div>
            <div class="form-group">
                {!!  Form::label(trans('general.address'),null,array('class'=>'control-label')) !!}
                {!! Form::textarea('address',$user->address,array('class'=>'form-control wysihtml5','rows'=>'3')) !!}
            </div>
            <div class="form-group">
                {!!  Form::label(trans_choice('general.note',2),null,array('class'=>'control-label')) !!}

                {!! Form::textarea('notes',$user->notes,array('class'=>'form-control wysihtml5','rows'=>'3')) !!}
            </div>
        </div>
        <div class="panel-footer">
            <div class="heading-elements">
                <button type="submit" class="btn btn-primary pull-right">{{trans_choice('general.save',1)}}</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview')
                        .attr('src', e.target.result)
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
@section('footer-scripts')
    <script src="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
@endsection