@extends('layouts.master')
@section('title')
    {{trans_choice('general.edit',1)}} {{trans_choice('general.charge',1)}}
@endsection
@section('content')


@if($charge->penalty==0)

<style type="text/css">
    
#penaltyStartDateDiv{
    display: none;
}

    
#penaltyHowManyDays{
    display: none;
}

    
#penaltyInterest{
    display: none;
}
    
#interestPerPeriod{
    display: none;
}


</style>

@endif

@if($charge->penalty_per_the_number_of_days==1)

<style type="text/css">
    

    
#interestPerPeriod{
    display: none;
}


</style>

@endif


    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">{{trans_choice('general.edit',1)}} {{trans_choice('general.charge',1)}}</h6>

            <div class="heading-elements">

            </div>
        </div>
        {!! Form::open(array('url' => url('charge/'.$charge->id.'/update'), 'method' => 'post', 'class' => 'form-horizontal')) !!}
        <div class="panel-body">
            <div class="form-group">
                {!! Form::label('name',trans_choice('general.name',1)." *",array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::text('name',$charge->name, array('class' => 'form-control', 'placeholder'=>"",'required'=>'required')) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('product',trans_choice('general.product',1)." *",array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::text('product',$charge->product, array('class' => 'form-control', 'id'=>"product",'required'=>'required','readonly'=>'readonly')) !!}
                </div>
            </div>
            <div class="form-group" id="loanChargeTypeDiv">
                {!! Form::label('loan_charge_type',trans_choice('general.charge',1)." ".trans_choice('general.type',1),array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::select('loan_charge_type',['disbursement'=>trans_choice('general.disbursement',1),'specified_due_date'=>trans_choice('general.specified_due_date',2),'installment_fee'=>trans_choice('general.installment_fee',2),'overdue_installment_fee'=>trans_choice('general.overdue_installment_fee',2),'loan_rescheduling_fee'=>trans_choice('general.loan_rescheduling_fee',2),'overdue_maturity'=>trans_choice('general.overdue_maturity',2)],$charge->charge_type, array('class' => 'form-control', 'id'=>"loan_charge_type",'required'=>'required')) !!}
                </div>
            </div>
            <div class="form-group" id="savingsChargeTypeDiv">
                {!! Form::label('savings_charge_type',trans_choice('general.charge',1)." ".trans_choice('general.type',1),array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::select('savings_charge_type',['specified_due_date'=>trans_choice('general.specified_due_date',2),'savings_activation'=>trans_choice('general.savings_activation',2),'withdrawal_fee'=>trans_choice('general.withdrawal_fee',2),'annual_fee'=>trans_choice('general.annual_fee',2),'monthly_fee'=>trans_choice('general.monthly_fee',2)],$charge->charge_type, array('class' => 'form-control', 'id'=>"savings_charge_type",'required'=>'required')) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('amount',trans_choice('general.amount',1)." *",array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::text('amount',$charge->amount, array('class' => 'form-control touchspin', 'id'=>"amount",'required'=>'required')) !!}
                </div>
            </div>

            <div class="form-group" id="loanChargeOptionDiv">
                {!! Form::label('loan_charge_option',trans_choice('general.charge',1)." ".trans_choice('general.option',1),array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::select('loan_charge_option',['fixed'=>trans_choice('general.fixed',1),'principal_due'=>trans_choice('general.principal',1).' '.trans_choice('general.due',1),'principal_interest'=>trans_choice('general.principal',1).' + '.trans_choice('general.interest',1).' '.trans_choice('general.due',1),'interest_due'=>trans_choice('general.interest',1).' '.trans_choice('general.due',1),'total_due'=>trans_choice('general.total',1).' '.trans_choice('general.due',1),'original_principal'=>trans_choice('general.original',2).' '.trans_choice('general.principal',1)],$charge->charge_option, array('class' => 'form-control', 'id'=>"loan_charge_option",'required'=>'required')) !!}
                </div>
            </div>
            <div class="form-group" id="savingsChargeOptionDiv">
                {!! Form::label('savings_charge_option',trans_choice('general.charge',1)." ".trans_choice('general.option',1),array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::select('savings_charge_option',['fixed'=>trans_choice('general.fixed',1),'percentage'=>trans_choice('general.percentage',1)],$charge->charge_option, array('class' => 'form-control', 'id'=>"savings_charge_option",'required'=>'required')) !!}
                </div>
            </div>
            <div class="form-group" id="penaltyDiv">
                {!! Form::label('penalty',trans_choice('general.penalty',1),array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::select('penalty',['1'=>trans_choice('general.yes',1),'0'=>trans_choice('general.no',2)],$charge->penalty, array('class' => 'form-control', 'id'=>"penalty",'required'=>'required')) !!}
                </div>
            </div>


            <div class="form-group" id="penaltyStartDateDiv">
                {!! Form::label('penalty_start_from',trans_choice('general.penalty_start_from',1),array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::number('penalty_start_from',$charge->penalty_start_from, array('class' => 'form-control', 'placeholder'=>"")) !!}
                </div>
            </div>

            <div class="form-group" id="penaltyHowManyDays">
                {!! Form::label('penalty_duration',trans_choice('general.penalty_duration',1),array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::number('penalty_duration',$charge->penalty_duration, array('class' => 'form-control', 'placeholder'=>"")) !!}
                </div>
            </div>

            <div class="form-group" id="penaltyInterest">
                {!! Form::label('penalty_per_the_number_of_days',trans_choice('general.penalty_per_the_number_of_days',1),array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::select('penalty_per_the_number_of_days',['1'=>trans_choice('general.no',1),'0'=>trans_choice('general.yes',2)],$charge->penalty_per_the_number_of_days, array('class' => 'form-control', 'id'=>"penalty_per_the_number_of_days",'required'=>'required','onchange'=>'myFunction2()')) !!}
                </div>
            </div>

            <div class="form-group" id="interestPerPeriod">
                {!! Form::label('penalty_interest_per_period',trans_choice('general.penalty_interest_per_period',1),array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::select('penalty_interest_per_period',['daily'=>trans_choice('general.daily',1),'weekly'=>trans_choice('general.weekly',2),'monthly'=>trans_choice('general.monthly',2),'yearly'=>trans_choice('general.yearly',2)],$charge->penalty_interest_per_period, array('class' => 'form-control', 'id'=>"penalty_interest_per_period",'required'=>'required')) !!}
                </div>
            </div>



            <div class="form-group">
                {!! Form::label('active',trans_choice('general.active',1),array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::select('active',['1'=>trans_choice('general.yes',1),'0'=>trans_choice('general.no',2)],$charge->active, array('class' => 'form-control', 'id'=>"active",'required'=>'required')) !!}
                </div>
            </div>
            <div class="form-group" id="overrideDiv">
                {!! Form::label('override',trans_choice('general.override',1),array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::select('override',['1'=>trans_choice('general.yes',1),'0'=>trans_choice('general.no',2)],$charge->override, array('class' => 'form-control', 'id'=>"override",'required'=>'required')) !!}
                </div>
            </div>
        </div>
        <!-- /.panel-body -->
        <div class="panel-footer">
            <div class="heading-elements">
                <button type="submit" class="btn btn-primary pull-right"> {{trans_choice('general.save',1)}}</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection
@section('footer-scripts')
    <script>

                function myFunction() {
            
            var x = document.getElementById("penalty").value;
            if(x==1){
                $("#penaltyStartDateDiv").css("display", "block");
                $("#penaltyHowManyDays").css("display", "block");
                $("#penaltyInterest").css("display", "block");
            }else{
                $("#penaltyStartDateDiv").css("display", "none");
                $("#penaltyHowManyDays").css("display", "none");
                $("#penaltyInterest").css("display", "none");
                $("#interestPerPeriod").css("display", "none");
            }
        }

        function myFunction2() {
            var x = document.getElementById("penalty_per_the_number_of_days").value;
            if(x==0){
                $("#interestPerPeriod").css("display", "block");
            }else{
                $("#interestPerPeriod").css("display", "none");
            }
            
        }


        $(document).ready(function (e) {
            if ($('#product').val() === "loan") {
                $('#loanChargeTypeDiv').show();
                $('#loanChargeOptionDiv').show();
                $('#penaltyDiv').show();
                $('#overrideDiv').show();
                $('#savingsChargeTypeDiv').hide();
                $('#savingsChargeOptionDiv').hide();
            } else {
                $('#savingsChargeTypeDiv').show();
                $('#savingsChargeOptionDiv').show();
                $('#loanChargeTypeDiv').hide();
                $('#loanChargeOptionDiv').hide();
                $('#penaltyDiv').hide();
                $('#overrideDiv').hide();

            }
            $('#product').change(function () {
                if ($('#product').val() === "loan") {
                    $('#loanChargeTypeDiv').show();
                    $('#loanChargeOptionDiv').show();
                    $('#penaltyDiv').show();
                    $('#overrideDiv').show();
                    $('#savingsChargeTypeDiv').hide();
                    $('#savingsChargeOptionDiv').hide();
                } else {
                    $('#savingsChargeTypeDiv').show();
                    $('#savingsChargeOptionDiv').show();
                    $('#loanChargeTypeDiv').hide();
                    $('#loanChargeOptionDiv').hide();
                    $('#penaltyDiv').hide();
                    $('#overrideDiv').hide();

                }
            })
        })
    </script>
@endsection