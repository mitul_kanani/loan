@extends('layouts.master')
@section('title')
    {{'Omit'}} {{'Charges'}}
@endsection
@section('content')
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">{{'Show only the current penalty for'}} 
            <span style="color:brown">{{\App\Models\LoanProduct::where('id',$loan->loan_product_id)->first()->name}}</span></h6>

            <div class="heading-elements">

            </div>

            <?php 

                $get_loan_product_details = \App\Models\LoanProduct::where('id',$loan->loan_product_id)->first()->show_just_current_penalty;

             

             ?>

          

            <form action="{{ url('loan/'.$loan->id.'/omit_charges_update') }}" method="POST">
                

            <div class="form-group">
                
               
                    <select class="form-control" id="active" required="required" name="active">


                        <option value="0"

                        <?php 
                            if($get_loan_product_details==0){
                                echo 'selected="selected"';
                            }

                         ?>

                        >No</option>
                        <option value="1"

                        <?php 
                            if($get_loan_product_details==1){
                                echo 'selected="selected"';
                            }

                         ?>

                        >Yes</option>
                        
                    </select>
               
                
                
            </div>
           
   
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
            <div class="form-group">
             <input id="btnSubmit" class="btn btn-success btn-xs" type="submit" value="Update" />
             </div>
            </form>

             </div>

             

        </div>
       


    </div>
    <!-- /.box -->

@endsection

